\section{Selection and Implementation of Trackers}
In order to effectively assess the viability of visual object tracking algorithms on SPAD camera data, we selected a representative sample of trackers for comparison. The trackers selected were Meanshift \cite{kernel-tracking}, KCF \cite{henriques-kcf} and STRUCK \cite{struck}, as these represent the dominant state-of-the-art approaches taken towards visual object tracking. While it should be noted that there have been more recent developments in higher performant trackers \cite{vot2016}, many of these build on the techniques and approaches that were developed in the selected trackers. One example of this is that the selected trackers do not provide a way to detect when the target has been lost. If there is any occlusion or the target moves out of the frame, then they also have no ability to re-capture the target when it becomes visible again. While extensions to these trackers have been developed to incorporate these features, these are outside of the scope of this project. Thus, we do not expect the algorithms selected to provide the best possible results, but will rather provide an indication as to which family of algorithms is better suited to tracking objects using the SPAD camera data. Using this information, we can guide the research in this area towards algorithms that are more likely to succeed, and avoid expending effort on those that show little potential.

An evaluation suite was developed in \textsc{Matlab} to perform the testing of each tracker\footnote{All code for this project is available at https://bitbucket.org/jackhenderson101/engn3712-code/}. Implementations for both Meanshift and KCF were created, based off the original authors' specifications and example code. These were tested and compared to the existing implementations to confirm that they matched the algorithms' stated performance. In the case of STRUCK, the original C++ code was used and a \textsc{Matlab} wrapper was written to interface it to the evaluation tools. Due to the complexity of the tracker and also the performance requirements, it was decided that re-implementing the STRUCK tracker in \textsc{Matlab} posed too great of a risk, both in terms of potential for errors and in performance losses.

Following the same approach as the VOT2016 challenge \cite{vot2016}, the estimated target location from a tracker is monitored while it is running on a video sequence. If the target drifts too far from the ground truth then the tracker is re-initialised with the current ground-truth location. This compensates for the trackers inability to detect and correct for loss-of-target situations. It also reduces the bias that video sequence length has on the results. For example, if a tracker loses the target at the beginning of a long sequence, it can be reset and still accurately track the target for the remainder of the sequence. Without resetting, the tracker would fail to track any subsequent frames and its performance would heavily depend on what time the target was lost. The number of times that a tracker needs to be reset can be recorded for each sequences and used a measure of robustness, with a lower number of resets indicating better performance. Ultimately, this method is only useful as an evaluation tool as it requires a pre-existing ground-truth trajectory for the target, which is not available in real-world tracking scenarios.


\section{Analysis on Benchmark Datasets}

Before attempting to run the trackers on the SPAD camera data, we first performed an analysis of their performance on standard RGB video sequences. The purpose of this is to provide a performance benchmark using sequences that these algorithms would typically be run on. Sequences were selected from different sources \cite{WuLimYang13, pets-dataset} focussing on those with low resolution, as this is one of the defining aspects of the SPAD camera data. Sequences with occlusion and target-out-of-frame characteristics were avoided as the primary focus was on simple tracking behaviour. A summary of each sequence is shown in Table~\ref{tab:benchmark-sequences}.

\begin{table}
	\renewcommand{\arraystretch}{1.25}
\begin{tabular}{|l|r|l|c|c|c|}
	\hline 
	Name & Frames & Camera & Image Dimensions & Average Target Size & Source \\ 
	\hline 
	Biker & 142 & Fixed & $360\times640$ & $24 \times 31$ & \cite{WuLimYang13} \\ 
	\hline 
	Box & 1161 & Fixed & $480 \times 640$ & $96 \times 122$ & \cite{WuLimYang13} \\ 
	\hline 
	Car1 & 1020 & Moving & $240 \times 320$ & $25 \times 21$ & \cite{WuLimYang13} \\ 
	\hline 
	egtest01 & 1821 & Moving & $480 \times 640$ & $33 \times 23$ & \cite{pets-dataset} \\ 
	\hline 
	egtest02 & 1301 & Moving & $480 \times 640$ & $33 \times 22$ & \cite{pets-dataset} \\ 
	\hline 
	egtest03 & 2571 & Moving & $480 \times 640$ & $34 \times 30$ & \cite{pets-dataset} \\ 
	\hline 
	Panda & 1000 & Fixed & $233 \times 312$ & $28 \times 21$ & \cite{WuLimYang13} \\ 
	\hline 
	RedTeam & 1918 & Moving & $240 \times 352$ & $37 \times 20$ & \cite{WuLimYang13} \\ 
	\hline 
	Surfer & 376 & Moving & $360 \times 480$ & $31 \times 37$ & \cite{WuLimYang13} \\ 
	\hline 
\end{tabular} 
\caption{Description of selected benchmark sequences}
\label{tab:benchmark-sequences}
\end{table}

\subsection{Metrics}
It can be difficult to objectively rate the performance of a tracker on a sequence of data. Ultimately, it depends on the intended application of the tracker, and what aspects of the tracker are more important for the particular application. Two of the most commonly used metrics are target location error, and target overlap \cite{vot2016, struck, henriques-kcf}, these provide two different but equally valuable measures of tracker performance. The target location error is simply the euclidean distance between the centre of the target and the centre of the ground truth. In many cases, pixels are used to measure the distance between the two points, however this creates a dependence on the resolution of the image, and the size of the target in the frame. In order to correct for this, the location error is normalised against the dimensions of the ground truth target:
\begin{gather}
\sqrt{\left( \frac{T_y - G_y}{G_h} \right)^2 +\left( \frac{T_x - G_x}{G_w} \right)^2}
\end{gather}
where $T$ is the target location determined by the tracker and $G$ is the ground truth value. Here, the subscripts $x$ and $y$ denote the x-coordinate and y-coordinate of the centre of the rectangle and the subscripts $w$ and $h$ denote the rectangle's width and height, respectively.

The target overlap metric measures the area of the intersection of the target and ground truth rectangles as a fraction of union of the areas. This allows the metric to incorporate information about how closely the shape and scale of the target matches the ground truth, rather than just its relative position. Similar to the normalised location error metric, this is invariant to the resolution of the image. However, as most of the datasets selected have small bounding boxes, there is unavoidable discretisation on the dimensions of the ground truth. This can mean an error of only one or two pixels can have a significant penalty on the value of the metric.

\subsection{Parameters and Features}
The three selected trackers were run on the datasets shown in Table \ref{tab:benchmark-sequences}. Parameters for each of the trackers were kept at the default values described in their respective papers \cite{kernel-tracking, henriques-kcf, struck}, with a detailed description of these parameters listed in Appendix \ref{app:parameters}. The features used in this trial were just the raw pixel values from the image. This provides a baseline indication of performance and allows comparisons to be made between the trackers without biasing the results to trackers that use better features. Also, because of the nature of the SPAD image data, it is likely that an entirely new feature will need to be created in order to ensure the best tracking performance.

\subsection{Results}
\subsubsection{Surfer Sequence}
\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{images/surfer}
	\caption{Key frames selected from `Surfer' sequence, showing the target identified by each tracker and the ground truth. (Best viewed in colour)}
	\label{fig:surfer}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{images/surfer-location-error}
	\caption{Normalised location error over time for `Surfer' sequence.}
	\label{fig:surfer-location-error}
\end{figure}


We highlight the `Surfer' sequence as a typical tracking sequence, with several key frames shown in Figure \ref{fig:surfer}. As shown, the ground truth follows the surfer's head and all trackers perform moderately well. Figure \ref{fig:surfer-location-error} shows the normalised location error over time, from which several observations can be made. STRUCK performs consistently well with a high percentage of frames having an error below 0.4. It was also not reset in this sequence, indicating that the tracker correctly tracked the target across every frame. Compared to this, the Meanshift tracker was reset 4 times, or at a rate of 1.06 resets per 100 frames. It also tracked the target less-precisely than the other two trackers, as the location error is consistently higher. The KCF tracker was often able to track the target more precisely than STRUCK, for example between frames 60 to 110, and 250 to 370. However, there was a section in-between where it drifted away from the ground truth and was eventually reset.


\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=\linewidth]{surfer-precision}
		\caption{Precision Curve}
		\label{fig:surfer-precision}
	\end{subfigure}%
	\begin{subfigure}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=\linewidth]{surfer-success}
		\caption{Success Curve}
		\label{fig:surfer-success}
	\end{subfigure}
	\caption{Precision and Success curves for `Surfer' sequence}
	\label{fig:surfer-precision-success}
\end{figure}

The performance of the trackers can also be evaluated using precision and success curves, see Figures \ref{fig:surfer-precision} and \ref{fig:surfer-success} respectively. The precision curve indicates the fraction of the frames where the location error was below the specified threshold. As the threshold increases, more frames meet the criteria and thus the curve monotonically increases towards 1. Steeper curves and higher values for lower thresholds indicate better tracking performance. The success curve measures the fraction of frames where the target overlap is above the specified threshold. In this case, flatter curves and high values for high thresholds indicate better tracking performance. In both cases, the closer the curve is to a horizontal line with value 1, the better the performance.

The precision and success curves for the `Surfer' sequence, shown in Figure \ref{fig:surfer-precision-success} reveal the same information that was previously discussed. The KCF tracker has a higher precision than STRUCK for approximately 70\% of the sequence, but on the whole sequence STRUCK provides better performance as indicated by the precision curve approaching 1 at a much lower threshold than for KCF. The success curve reveals one of the limitations of the STRUCK tracker used. While it may track the location of the target accurately, the implementation that we used does not have scale adaptation as part of the algorithm. This means that the size of the bounding box used by the tracker is fixed, and cannot adjust when the target changes in size. This can be observed in the success curve by the poor performance at thresholds above 0.5 and visually in frame 150 of Figure \ref{fig:surfer}. The STRUCK bounding box is wholly contained within the ground truth, but because of the incorrect scale, it is scored poorly on the target overlap metric. While scale adaptation was a feature that was added to STRUCK after the initial release \cite{struck}, it was not included in the software that was publicly available.

\subsubsection{Egtest02 Sequence}

\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{images/egtest02}
	\caption{Key frames selected from `egtest02' sequence, showing the target identified by each tracker and the ground truth. (Best viewed in colour)}
	\label{fig:egtest02}
\end{figure}


\begin{figure}
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\linewidth]{egtest02-precision}
		\caption{Precision Curve}
		\label{fig:egtest02-precision}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\linewidth]{egtest02-success}
		\caption{Success Curve}
		\label{fig:egtest02-success}
	\end{subfigure}
	\caption{Precision and Success curves for `egtest02' sequence}
	\label{fig:egtest02-precision-success}
\end{figure}

The `egtest02' sequence, shown in Figure \ref{fig:egtest02}, provides an example of a more challenging scenario. The target is much smaller, changes size dramatically, and there are other vehicles in the scene that pass near the target vehicle. At first glance, the precision and success curves for this sequence, shown in Figure \ref{fig:egtest02-precision-success}, indicate that the performance of KCF is significantly better than STRUCK or Meanshift. However, it should be noted that KCF and Meanshift were reset 10 times, and STRUCK was reset only 6 times. A notable example of where STRUCK outperforms KCF in this sequence is in frame 512, visible in Figure \ref{fig:egtest02}. Another vehicle passes behind the target travelling in the opposite direction. It has a higher contrast against the background compared to the desired target, and the KCF tracker mistakenly begins to track this vehicle. As the STRUCK tracker maintains a set of support vectors, describing the different appearances of the target, it is able to distinguish between the two vehicles, despite the bounding box encompassing both. STRUCK again suffers heavily in this sequence due to the failure to adapt to the decreasing target size.

\subsubsection{Overall Analysis}
\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{images/benchmark-precision}
	\caption{Combined precision plot for all benchmark sequences}
	\label{fig:benchmark-precision}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{images/benchmark-success}
	\caption{Combined success plot for all benchmark sequences}
	\label{fig:benchmark-success}
\end{figure}

\begin{table}
	\centering
	\renewcommand{\arraystretch}{1.25}
	\begin{tabular}{|c|c|c|c|}
		\hline 
		& Successfully Tracked & Successfully Tracked& Resets per  \\
		& Frames (LE\textless0.25) &  Frames (TO\textgreater0.4) & 100 frames\\
		\hline 
		Meanshift & 60.3\% & 55.9\% & 0.42 \\ 
		\hline 
		KCF & 58.4\% & 64.1\% & 0.25 \\ 
		\hline 
		STRUCK & 73.1\% & 69.3\% & 0.11 \\ 
		\hline 
	\end{tabular} 
	\caption{Summary of results from benchmark sequences}
	\label{tab:benchmark-summary}
\end{table}

Analysing the set of sequences as a whole provides a different, and somewhat surprising perspective. The precision and success curves shown in Figures \ref{fig:benchmark-precision} and \ref{fig:benchmark-success} respectively, clearly show that STRUCK performs the best out of the three. However, the distinction between KCF and Meanshift is not as clear. KCF tracks a larger percentage of the frames very accurately, as indicated by the high precision score at small thresholds, but falls short of Meanshift as the threshold is increased. This suggests that KCF is a more accurate tracker but fails more abruptly than Meanshift does. On the contrary, the success curves of both trackers show that KCF provides consistently better tracking than Meanshift, which may indicate that the deficiencies in tracking accuracy mentioned above are compensated for by better target scale adaptation. Interestingly, STRUCK again performs well on this metric even though it lacks the scale adaptation of the two other trackers.

Table \ref{tab:benchmark-summary} shows a summary of the algorithms' performance based on a single threshold value for the Location Error (LE) and the Target Overlap (TO) metrics. The reset rate per 100 frames gives an indication of the robustness of the tracker and shows that STRUCK performs best against this metric as well. As the reset rate of Meanshift is nearly double that of KCF, it will inherently have an advantage in terms of tracking accuracy. This suggests that despite the two trackers having similar precision curves, KCF is much more robust and without target resetting it would significantly outperform Meanshift.

\subsection{Validation}

The results obtained from these trials are broadly consistent with those discussed in the literature. Meanshift provides moderately good performance, but cannot match the newer and more advanced trackers like STRUCK and KCF. The literature is not as clear on the differences between STRUCK and KCF. As is typical, the authors of these algorithms claim that they can achieve better performance than the alternative \cite{henriques-kcf, struck}, but independent benchmarks \cite{vot2016} and the general consensus indicate that KCF is more performant than STRUCK. However, this is based on the best performing set of features for each tracker, rather than just the raw pixel values. Henriques et. al \cite{henriques-kcf}, authors of KCF, show that STRUCK does indeed perform better than KCF with raw pixel features. It also performs better than all feature variants of KCF on the 4 sequences they tested designated as low resolution.

It appears that the key feature of KCF that allows for its high performance is also one of its weaknesses. The utilisation of circulant matrices and Fourier transforms allows KCF to train on a much larger number of samples than other algorithms. However, as the size of the target decreases, this advantage diminishes and the trade-offs that have had to be made to achieve this become evident. STRUCK trains with much fewer samples, by taking a random selection in the neighbourhood of the target, but performs a much deeper analysis of each sample rather than just relying on the sheer volume of samples in the way that KCF does.
