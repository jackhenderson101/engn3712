\section{SPAD Data Modelling}

A limited set of SPAD sensor data was provided by the Defence Science and Technology Group (DSTG) for use in this project which contained data from 3 different scenarios, each with 55 recordings. In each scenario, a micro-UAV was positioned in front of the sensor and moved between two fixed points in a specified amount of time. This was repeated 55 times for each scenario, to generate a total of 165 sequences. A qualitative description of the trajectory was provided for each scenario, however a true, independent ground truth measurement for each sequence could not be obtained due to limitations of the experimental setup. Without a ground truth accompanying the sequences, the initialisation, target resetting, and most importantly the evaluation of the tracking algorithms could not be performed. Additionally, a hardware fault was identified in the camera, which suggested that the data received might have slightly different characteristics to true SPAD data. DSTG indicated that this fault would not significantly impact the overall properties of the data. 

In order to account for a lack of ground truth, a synthetic dataset was created. Using the data provided from DSTG, the characteristics of the SPAD sensor data were studied and then replicated to generate artificial data that matched the real-world data as closely as possible. The advantage of this approach is that the sequences can be generated directly from a ground truth specification, and thus allow the algorithms to be evaluated against the true target trajectory. It also allowed for arbitrary trajectories to be evaluated and modifications of the target size and sensor size to be made.

\subsection{SPAD Data Observations}

The sensor used to collect the DSTG data was a $32\times32$ pixel array. The values measured by the sensor are correlated with the time delay of a photon incident to that pixel, and thus are correlated with the depth of the object in that region of the scene. There are inherent limitations to the resolution of the depth measurements as they were recorded with 10 bits per pixel. Moreover, the values are based on the precise in measurement of the time of arrival of individual photons. In the experiments that were performed to create the dataset, the target's depth changed by approximately 10 cm, which is close to the lower limit of what the sensor can resolve. Thus, the data available only contains measurements in a small range of values, and hence we cannot draw any conclusions about how these measurements are affected by target distance.


\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{typical_spad_frame_movingAwayDark1_1634}
	\caption{An example of 3 consecutive frames from the DSTG dataset. (Frame: 1634,1635,1636, Seq: MovingAwayDark1)}
	\label{fig:typical_spad_frame}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{typical_spad_pixel_movingAwayDark1_full}
	\caption{Measurements from a single pixel in a sequence. (Pixel $(20,20)$, Seq: MovingAwayDark1)}
	\label{fig:typical_spad_pixel_full}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{typical_spad_pixel_movingAwayDark1_sawtooth}
	\caption{Measurements from two neighbouring pixels showing sawtooth phenomenon. (Seq: MovingAwayDark1)}
	\label{fig:typical_spad_pixel_sawtooth}
\end{figure}


Observation of the DSTG dataset indicated that there are two distinct phenomena; the low-amplitude background noise and the discrete high-amplitude pulses which form the signal. Figure \ref{fig:typical_spad_pixel_full} shows the behaviour of a single pixel in the SPAD array. The background noise is visible in the range $[990, 1023]$ and appears to be both spatially and temporally correlated. The `sawtooth' behaviour of this noise can be observed in Figure \ref{fig:typical_spad_pixel_sawtooth} and is most likely an artefact of the sensor electronics. This observation was confirmed with DSTG. When compared to the overall signal, this noise has a relatively insignificant magnitude and appears to be independent of the other elements of the signal. It's likely that pre-processing would remove this noise before tracking algorithms were applied, and thus we do not consider the effect of this noise in our model.

The primary component of the signal is a set of discrete pulses that occur in the range of $[400,600]$. It is believed that these pulses correspond to individual photons incident to that pixel. As seen in Figure \ref{fig:typical_spad_pixel_full}, the frequency of these peaks changes over time, as does their value. The difference in peak value is caused by variation in the arrival time of photons to the sensor while the change in the frequency of pulses is due to the target moving across the frame. The frequency of the pulses increases as the target moves into the region that is observed by this pixel. There is also a significant amount of erroneous pulses when the target is not observed by the pixel, e.g. Frame 4000 onwards. These may be caused by stray photons from unintended reflections or other light sources. These phenomena can be observed in the spatial dimensions in Figure \ref{fig:typical_spad_frame}, which shows 3 single frames of the data. The target is approximately in the lower centre of the frame, but there are a number pulses outside the target region. This makes it difficult to exactly determine the target location using just a single frame of data.

\subsection{Creating Synthetic SPAD Sequences}

To create synthetic sequences, we model the data with two distributions for each pixel; the value of each pulse and the number of frames between consecutive pulses. In order to do this, we make two main assumptions. The first is that the distributions are independent of time, which only holds if the target does not move in the frame. The second assumption is that samples from adjacent pixels are independent of each other. This is a much more complex condition to verify as there is an overall correlation between adjacent pixels because they are measuring adjacent regions in space. If the target is observed by a particular pixel, then it's likely that the target will also be observed by adjacent pixels. However, in terms of the value of the pulses measured and the interval between consecutive pulses, the assumption is that these are spatially independent. Initial observations of the data suggest that this is true, however a more rigorous analysis is required to confirm this assumption. One possible approach would be to use the Hilbert-Schmidt Independence Criterion (HSIC) \cite{hsic} as a measure of whether the samples are independent.

Another assumption that we make is that the background noise is independent of the target location. This is not particularly realistic as, for example, photons from the target will be reflected by background objects, and this depends on the presence of the target in a particular location. Further to this, we also assume that the appearance of the target remains constant throughout the sequence. In practice, as the target moves in the frame there may be out-of-plane rotations, which will change the observed characteristics of the target. Both of these assumptions are necessary as they remove a significant level of complexity into the model and the DSTG dataset is not of high enough quality to facilitate modelling these factors. 

Initially, the plan was to use an additional dataset from the SPAD sensor in which the target is stationary in the frame for the entire sequence. Using this, we would be able to determine the distributions for pixels that measure the background and those that measure the foreground (region containing the target). However, due to technical issues with the SPAD sensor that were outside of our control, this dataset was not made available within the time frame required by this project. Additionally, it was discovered that the original sequences received may also contain invalid data due to a fault in the sensor. Based on the advice that the fault was only minor, and that the properties of the data would be broadly similar, we continued with the analysis using the original data. However, any conclusions drawn from using this SPAD data must be conditioned on the fact the true SPAD sensor data may have slightly different properties.

As a sequence with a stationary target was not available, a subsequence of data from the existing dataset was identified where the target was stationary for several hundred frames. Frames 1 to 1000 of the ``movingAwayDark01'' sequence were selected and the location of the target was identified as $(T_x,T_y,T_w,T_h) = (24,14,14,10)$. Using this subsequence, the distributions of peak value and consecutive peak times were evaluated for each pixel in the target region. Pixels not in the target region were considered background, and a single pair of distributions for all background pixels was evaluated.

The synthetic sequences were created by sampling from these distributions using the method of inverse transform sampling. Firstly, a sequence of frames was generated containing only background pixels. For each pixel in the frame, a set of i.i.d. samples were drawn from the value distribution and the consecutive pulse distribution. Using these samples, the pulses were added into the sequence at the appropriate times. Independent of this, a target sequence was created with dimensions equal to the original target's width and height. Using the individual distributions for each pixel, the sequence of target frames was created in a similar way to the background. The target sequence was then overlaid onto the background in the location specified by the ground truth trajectory. Figure \ref{fig:synthetic-process} provides a graphical description of this process.

\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{images/synthetic-process}
	\caption{Graphical representation of the creation of a synthetic frame}
	\label{fig:synthetic-process}
\end{figure}


