\section{Analysis on SPAD Datasets}
As a model of the SPAD sensor data had been created, we were now able to create arbitrary datasets with a range of different trajectories and their accompanying ground truth bounding boxes. Five different sequences were created, each 1000 frames long and with a different trajectory. The specification of each of these sequences is provided in Table~\ref{tab:spad-sequences}. A mix of simple linear and circular trajectories were selected to ensure that the trackers encountered a range of different target behaviours. The size of the frame was expanded from the original resolution of $32\times32$ to $50\times50$, while keeping the size of the target the same and its location within the original $32\times32$ region. This was to prevent the trackers from hitting the edges of the frame when the target is close to the boundary. If the size of the frame were not increased, trackers that reach the edge will be constrained to stay within the frame. This may result in better tracking accuracy than it would otherwise be observed if the target location wasn't constrained. As the target is modelled from an existing SPAD sequence, it retains the dimensions of that sample. In this case, the target's width and height are 10 and 14 pixels respectively.

\begin{table}
	\centering
		\renewcommand{\arraystretch}{1.4}
	\begin{tabular}{|c|c|c|c|l|}
		\hline 
		Seq. & Initial Location & Final Location & Trajectory & Description \\ 
		\hline 
		1 & $(15,15)$ & $(35,35)$ & Linear & Top Left to Lower Right \\ 
		\hline 
		2 & $(15,25)$ & $(35,25)$ & Linear & Vertical through centre \\ 
		\hline 
		3 & $(25,15)$ & $(25,35)$ & Linear & Horizontal through centre \\ 
		\hline 
		4 & $(40,25)$ & $(40,25)$ & Circular  & Anti-clockwise around centre \\ 
		\hline 
		5 & $(40,25)$ & $(40,25)$ & Circular & Clockwise around centre \\ 
		\hline 
	\end{tabular} 
	\caption{Specification of synthetic SPAD datasets created.}
	\label{tab:spad-sequences}
\end{table}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.49\textwidth}

		\includegraphics[width=\textwidth]{precision_spad_raw}
		\caption{Raw feature}
		\label{fig:precision-spad-raw}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}

	\includegraphics[width=\textwidth]{precision_spad_raw_3x}
	\caption{Raw feature with $3\times$ scale}
	\label{fig:precision-spad-raw-3x}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}

	\includegraphics[width=\linewidth]{precision_spad_peak_5}
	\caption{Peak-count feature over 5 frames}
	\label{fig:precision-spad-peak-5}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}

	\includegraphics[width=\linewidth]{precision_spad_peak_10}
	\caption{Peak-count feature over 10 frames}
	\label{fig:precision-spad-peak-10}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
	\includegraphics[width=\linewidth]{precision_spad_peak_30}
	\caption{Peak-count feature over 30 frames}
	\label{fig:precision-spad-peak-30}
	\end{subfigure}
	\caption{Precision curves for total set of synthetic SPAD sequences}
	\label{fig:precision-spad}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		
		\includegraphics[width=\textwidth]{success_spad_raw}
		\caption{Raw feature}
		\label{fig:success-spad-raw}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		
		\includegraphics[width=\textwidth]{success_spad_raw_3x}
		\caption{Raw feature with $3\times$ scale}
		\label{fig:success-spad-raw-3x}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		
		\includegraphics[width=\linewidth]{success_spad_peak_5}
		\caption{Peak-count feature over 5 frames}
		\label{fig:success-spad-peak-5}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		
		\includegraphics[width=\linewidth]{success_spad_peak_10}
		\caption{Peak-count feature over 10 frames}
		\label{fig:success-spad-peak-10}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\linewidth]{success_spad_peak_30}
		\caption{Peak-count feature over 30 frames}
		\label{fig:success-spad-peak-30}
	\end{subfigure}
	\caption{Success curves for total set of synthetic SPAD sequences}
	\label{fig:success-spad}
\end{figure}

\begin{table}
	\centering
	\renewcommand{\arraystretch}{1.4}
	\begin{tabular}{|c|>{\centering}p{2cm}|>{\centering}p{2cm}|>{\centering}p{2cm}|c|}
		\hline 
		& Meanshift & KCF & STRUCK & BlobTrack \\ 
		\hline 
		Raw & 23.52 & 93.10 & 91.90 & --- \\ 
		\hline 
		Raw - 3x scale & 93.48 & 91.90 & 98.52 & --- \\ 
		\hline 
		Peak Count - 5 frames & 88.79 & 99.94 & 100 & 29.53 \\ 
		\hline 
		Peak Count - 10 frames & 97.86 & 77.19 & 89.69 & 59.10 \\ 
		\hline 
		Peak Count - 30 frames & 100 & 34.20 & 78.20 & 100 \\ 
		\hline 
	\end{tabular} 
	\caption{Percentage of successfully tracked frames where Location Error $<0.25$}
	\label{tab:spad-precision-summary}
\end{table}

\begin{table}
	\centering
	\renewcommand{\arraystretch}{1.4}
	\begin{tabular}{|c|>{\centering}p{2cm}|>{\centering}p{2cm}|>{\centering}p{2cm}|c|}
		\hline 
		& Meanshift & KCF & STRUCK & BlobTrack \\ 
		\hline 
		Raw & 50.72 & 99.18 & 99.24 & --- \\ 
		\hline 
		Raw - 3x scale & 99.88& 99.20 & 99.94 & --- \\ 
		\hline 
		Peak Count - 5 frames & 100 & 100 & 100 & 44.00 \\ 
		\hline 
		Peak Count - 10 frames & 100 & 92.53 & 99.74 & 87.20 \\ 
		\hline 
		Peak Count - 30 frames & 100 & 55.95 & 86.92 & 100 \\ 
		\hline 
	\end{tabular} 
	\caption{Percentage of successfully tracked frames where Target Overlap $>0.4$}
	\label{tab:spad-success-summary}
\end{table}

\subsection{Tracking with raw data}
The first test performed was to run the trackers on the raw, unmodified SPAD data. Precision and success curves are shown in figures \ref{fig:precision-spad-raw} and \ref{fig:success-spad-raw} respectively. Surprisingly, all trackers performed moderately well on the raw data. STRUCK and KCF performed similarly, with Meanshift unable to match their performance. Initial testing showed that the significantly lower resolution of the target may be having an effect on tracking performance. Thus, another test was run with the SPAD image being scaled up by a factor of 3. This improved the performance of all trackers, as can be seen in figures \ref{fig:precision-spad-raw-3x} and \ref{fig:success-spad-raw-3x}. Meanshift improved the most, bringing it to a similar performance level as KCF, which itself did not benefit significantly from the scale change. Despite the high noise levels that are not typically found in traditional visual object tracking sequences, all trackers can accurately and reliably tracking the synthetic SPAD target throughout the entire sequence. STRUCK has a slight edge over KCF and Meanshift, and with the $3\times$ scale change, tracks 98.5\% of frames with a location error of less than 0.25. In to the benchmark sequences tested previously, STRUCK only successfully tracked 73.1\% of frames according to the same criterion. This suggests that any of these trackers show potential towards successful tracking on real-world SPAD sensor data.

\subsection{Tracking with peak-count feature}
While tracking on the raw SPAD data proved successful, there is still opportunity for improvement. The more robust and accurate a tracker can be made on the synthetic data, the better chance it has when tracking more realistic data. Creating a feature that is invariant to the noise present in the SPAD data would provide a more stable input to the trackers, allowing them to track the target more accurately. Observing that the frequency of pulses in the signal is highly correlated with the location of the target, we propose a simple `peak-count' feature. For each pixel, we detect peaks by observing when the value is below 950, which is sufficient to eliminate the background noise. We then count the number of these peaks in the last $n$ frames of the sequence and use this as the feature value. We also retain the $3\times$ scale factor used with the raw features. A larger choice for $n$ will result in a more time-invariant feature, however it will lag behind the true target location, as it is relying on frames further in the past. If the target velocity is small then this effect is diminished. We test this feature with three different values of $n$ -- 5, 10 and 30 -- to examine what effect this has on tracking performance. An example frame for each of these features is shown in figure \ref{fig:spad_feature}.  

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.24\textwidth}
		
		\includegraphics[width=\textwidth]{spad_feature_raw}
		\caption{Raw feature}
		\label{fig:spad_feature_raw}
	\end{subfigure}
	\begin{subfigure}[b]{0.24\textwidth}
	
	\includegraphics[width=\textwidth]{spad_feature_5}
	\caption{$n=5$}
	\label{fig:spad_feature_5}
	\end{subfigure}
	\begin{subfigure}[b]{0.24\textwidth}
	
	\includegraphics[width=\textwidth]{spad_feature_10}
	\caption{$n=10$}
	\label{fig:spad_feature_10}
	\end{subfigure}
	\begin{subfigure}[b]{0.24\textwidth}
	
	\includegraphics[width=\textwidth]{spad_feature_30}
	\caption{$n=30$}
	\label{fig:spad_feature_30}
	\end{subfigure}
	\caption{Example of peak-count feature}
	\label{fig:spad_feature}
\end{figure}

Initial observations of the peak-count feature suggest that even a simple tracking-by-detection approach could be used to track the target. Considering this, we implement a simple tracker which determines the target location by computing the spatially-weighted average of the peak-count feature. In other words, it computes the centre of mass of the image. While this type of tracker has a vast number of potential drawbacks, it can be used as a baseline performance indicator to compare with the other trackers. If a given tracker fails to outperform this rudimentary algorithm, then it is clearly below the acceptable standard. We name this tracker `BlobTrack' and include its results alongside the other three other trackers being tested to provide a baseline comparison.

Tracking results for the peak-count feature are shown in figures \ref{fig:precision-spad} and \ref{fig:success-spad}, as well as in tables \ref{tab:spad-precision-summary} and \ref{tab:spad-success-summary}. Interestingly, this feature affects each tracker differently. Starting at $n=5$, we observe that Meanshift actually performs worse than the raw feature at 3x scale with successfully tracked frames dropping from 93.5\% to 88.8\%. However, increasing $n$ to 10 and further to 30 yields significant benefits for Meanshift. Successfully tracked frames increases to 97.9\% for $n=10$, and 100\% for $n=30$. This clearly indicates that the peak-count feature provides a clear and significant performance improvement to the Meanshift tracker. In the synthetic data used, target movement was small enough to have a negligible lag effect on the target location.

The results for KCF and STRUCK are surprisingly different to what was expected. For $n=5$, the peak-count feature provides a small, but significant improvement both trackers' performance. For KCF, the number of successfully tracked frames increases from 91.9\% to 99.9\% when compared to the raw feature, and STRUCK increases from 98.5\% to 100\% by the same measure. However, beyond $n=5$, the performance of both trackers decreases dramatically. At $n=30$, KCF only successfully tracks 34.2\% of frames, which is the lowest score of any tracker, excluding BlobTrack, for any feature type. Performance for STRUCK is also degraded but not as severely, with successfully tracked frames dropping to 78.2\%.

While these results are contrary to initial expectations, we theorise that the difference is due to the learning aspect of the KCF and STRUCK compared to Meanshift. In STRUCK and KCF, a model of the target is learned over time, and this is used to locate the target in subsequent frames. The more information that is used to train these models, the better performance will be. In the case of the peak-count feature, we predict that using a small value for $n$ is useful to filter out background noise. However, increasing this value too much causes the feature to average out and discard data that is useful in training the models. Thus, we can observe that performance for both STRUCK and KCF diminishes as $n$ increases as they are being trained on less informative data. For the more simple trackers, Meanshift and BlobTrack, performance increases as the target is more clearly distinguished from the background.

%Both of these results are completely counter-intuitive to what was expected. Figure \ref{fig:spad_feature_30} clearly shows a recognisable target that should be a comparatively easy target to track and yet both KCF and STRUCK suffer degraded performance compared to even the raw feature.

% The cause of this anomaly warrants further investigation as, intuitively, this behaviour should not occur. The fact that it affects both KCF and STRUCK, but not Meanshift further adds to the mystery. It's unlikely that there is an error in the implementation, as STRUCK was independently implemented directly by the authors, and any errors should have been borne out in previous trials. Another possibility is that the parameters of each tracker need to be modified in order to better suit the data from the new feature, but again it is unclear why it should perform well when $n=5$, but not when $n=30$. It can also not be attributed to the temporal lag in the feature location as BlobTrack show very good performance, and hence indicates that the feature is very close to the ground truth.

Overall, we can conclusively say that STRUCK performs the best out of the three trackers, with KCF lagging slightly behind. The best feature for these two trackers was the peak-count feature with $n=5$. Meanshift performed best when using the peak-count feature with $n=30$, but lagged behind the other trackers in most cases. The results show that the addition of the peak-count feature can improve tracking performance when compared to the raw feature, however this is heavily dependent on the number of frames included in the peak counting. Robustness does also not appear to be an issue, as there were no target resets in the entire sequence.



While there are a number of limitations and simplifications inherent to using the synthetic SPAD data, the results obtained have been valuable in a number of ways. Firstly, we can confirm that all trackers are able to accurately and robustly track a target with SPAD data. We have also identified the best performing tracker as well as the most suitable feature to use for each tracker. We discover that a simple tracking-by-detection approach can achieve comparable results to even the best tracker. All of these results suggest that all three trackers have the potential to perform well on more challenging datasets, and potentially on real-world SPAD sequences.



\subsection{Tracking on DSTG Dataset}
As we have established that all three trackers are more than capable of tracking the target on the synthetic data, we can provide a brief analysis on their performance on the original DSTG dataset. As discussed previously, without a ground truth reference for the dataset, a quantitative analysis cannot be performed. However, as we can observe in figures \ref{fig:precision-spad-peak-30} and \ref{fig:success-spad-peak-30}, BlobTrack comes very close to tracking the target near-perfectly. We can therefore use BlobTrack as a pseudo-ground truth reference in order to evaluate the metrics. There are inherent dangers in trusting BlobTrack as the ground truth, and thus we will not present any quantitative results such as the percentage of successfully tracked frames. In addition to this, we do not perform target resetting on the trackers as we cannot accurately reinitialise the trackers with the correct target location. We also cannot report on the target overlap metric, as the target changes size in the sequence and BlobTrack only tracks a single point, rather than a bounding box.
\begin{figure}
	\centering
	\includegraphics[width=0.9\textwidth]{real-spad-samples}
	\caption{Key frames selected from `MovingAwayDark1' sequence with tracking results. Frames visualised with peak-count feature ($n=5$). (Best viewed in colour)}
	\label{fig:real-spad-samples}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{real-spad-location-error}
    \caption{Non-normalised Location Error of each tracker over time on the MovingAwayDark1 sequence, using BlobTrack as the ground truth.}
    \label{fig:real-spad-location-error}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\linewidth]{real-spad-precision}
		\caption{All frames}
		\label{fig:real-spad-precision-all}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\linewidth]{real-spad-precision-7500}
		\caption{Up to frame 7500}
		\label{fig:real-spad-precision-7500}
	\end{subfigure}
    \caption{Non-normalised precision curves for MovingAwayDark1 sequence.}
    \label{fig:real-spad-precision}
\end{figure}



Figures \ref{fig:real-spad-samples}, \ref{fig:real-spad-location-error} and \ref{fig:real-spad-precision} show the raw results of running the trackers on a real SPAD dataset, while using BlobTrack as the ground truth. Beyond frame 5000, the target size begins to decrease, which means the ground truth values generated from BlobTrack are more sensitive to the noise in the signal, and thus the target location is not as consistent. This can be observed in the second half of the sequence, where the variance in error increase significantly, shown in figure  \ref{fig:real-spad-location-error}. Overall, it shows impressive performance from all three trackers, which generally are able to track the target through the whole sequence. The only exception is KCF, which appears to lose the target twice, at frames 7685 and 9952. However, in the first case, it appears to re-capture the target at frame 9444 without any external resetting taking place, although this is most likely a coincidence rather than a true re-capturing. This long period of target loss for KCF explains the plateau observed in figure \ref{fig:real-spad-precision-all}. Comparing the trackers only up to frame 7500 yields a more indicative comparison of performance, as shown in figure \ref{fig:real-spad-precision-7500}. However, without a reliable ground truth, the conclusions we can draw from this experiment are limited to general observations.